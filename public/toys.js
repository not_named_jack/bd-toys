function generate_table_column(value) {
    var column = document.createElement('td'); 
    column.appendChild(document.createTextNode(value));
    return column;
}

function generate_table_heading(value) {
    var column = document.createElement('th'); 
    column.appendChild(document.createTextNode(value));
    return column;
}

function first_table_row(keys) {
    var first_row = document.createElement('tr');

    first_row.appendChild(generate_table_heading('size'));
    first_row.appendChild(generate_table_heading('name'));

    for (var i = 0; i < keys.length; i++) {
        first_row.appendChild(
                generate_table_heading(keys[i])
                );
    }

    return first_row;
}

function toy_row(toy, keys) {
    var row = document.createElement('tr');

    row.appendChild(generate_table_column(toy['size']));
    row.appendChild(generate_table_column(toy['name']));

    for (var i = 0; i < keys.length; i++) {
        var dimension_value = toy['dimensions'][keys[i]];
        if (dimension_value !== undefined) {
            row.appendChild(generate_table_column(dimension_value.toFixed(1)));
        } else {
            row.appendChild(generate_table_column(''));
        }
    }

    return row;
}

function create_toy_table(keys, sort_by) {
    var reverse = document.getElementById('descending').checked;
    var sorted_toys = sort_toy_data(sort_by, reverse);
    var hide_empty_toys = document.getElementById('hide-empty').checked;
    
    var toy_table = document.createElement('table');

    toy_table.appendChild(first_table_row(keys));

    for (var i = 0; i < sorted_toys['list'].length; i++ ) {
        if (toy_has_keys(sorted_toys['list'][i], keys) || !hide_empty_toys) {
            toy_table.appendChild(toy_row(sorted_toys['list'][i], keys));
        }
    }

    return toy_table;
}

function toy_has_keys(toy, keys) {
    for (var i = 0; i < keys.length; i++) {
        if (!toy['dimensions'].hasOwnProperty(keys[i])) {
            return false;
        }
    }

    return true;
}

function sort_toy_data(sort_by, reverse) {
    var new_toys = JSON.parse(JSON.stringify(TOY_DATA));
    new_toys['list'].sort(function(a, b) {
        if (reverse) {
            [a, b] = [b, a];
        }

        if (!isFinite(a['dimensions'][sort_by]) && !isFinite(b['dimensions'][sort_by])) {
            return 0;
        }
        if (!isFinite(a['dimensions'][sort_by])) {
            return reverse ? -1 : 1;
        }
        if (!isFinite(b['dimensions'][sort_by])) {
            return reverse ? 1 : -1;
        }

        return a['dimensions'][sort_by] - 
            b['dimensions'][sort_by];
    });

    return new_toys;
}

function refresh_table() {
    var keys = document.getElementById('keys').value.replace(/ /g, '').split(',');

    // We are not doing this that often so performance is not that important
    if (JSON.stringify(keys) === JSON.stringify([""])) {
        replace_table(TOY_DATA['keys']);
    } else if (check_valid_keys(keys)) {
        replace_table(keys);
    } else {
        alert('Invalid keys');
    }

}

function replace_table(keys) {
    document.getElementById('toy-table').innerHTML = '';

    var sort_by = document.getElementById('sort-by').value;

    document.getElementById('toy-table').appendChild(
            create_toy_table(keys, sort_by));
}

function check_valid_keys(keys) {
    for (var i = 0; i < keys.length; i++ ) {
        if (TOY_DATA['keys'].indexOf(keys[i]) === -1) {
            return false;
        }
    }

    return true;
}

function valid_keys() {
    var strong = document.createElement('strong');
    strong.appendChild(document.createTextNode(TOY_DATA['keys'].join(', ')));

    var paragraph = document.createElement('p');
    paragraph.appendChild(document.createTextNode('Valid keys are: '));

    paragraph.appendChild(strong);
    return paragraph;
}

function sort_options() {
    var select = document.createElement('select');
    select.id = 'sort-by';
    for (var i = 0; i < TOY_DATA['keys'].length; i++ ) {
        var option = document.createElement('option');
        option.value = TOY_DATA['keys'][i];
        option.appendChild(document.createTextNode(TOY_DATA['keys'][i]));
        select.appendChild(option);
    }

    return select;
}

document.getElementById('sort-by-container').appendChild(sort_options());
document.getElementById('valid-keys').appendChild(valid_keys());
document.getElementById('toy-table').appendChild(
        create_toy_table(TOY_DATA['keys'], 'calculated-usable-volume')
        );

document.getElementById('key-form')
    .addEventListener('submit', function(event) {
        event.preventDefault();
        refresh_table();
    },
    false
);

