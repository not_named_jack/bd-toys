import json
import math

def toy_to_metric(toy):
    for size in toy:
        for property in toy[size]:
            toy[size][property] *= 2.54

    return toy

def toys_to_metric(toys):
    for toy in toys:
        toys[toy] = toy_to_metric(toys[toy])

    return toys

# Takes a single size for a toy
def get_usable_volume_for_size(toy):
    shaft_types = ['diameter-of-shaft',
            'diameter-of-bottom',
            'diameter-of-largest-part-of-shaft',
            'diameter-of-thickest-part-of-shaft',
            'diameter']

    length_types = ['usable-length',
            'usuable-length']

    usable_length = 0

    for length_type in length_types:
        try:
            usable_length = toy[length_type]
            break
        except KeyError:
            pass

    radius = 0

    for shaft_type in shaft_types:
        try:
            radius = toy[shaft_type] / 2
            break
        except KeyError:
            pass

    return math.pi * radius ** 2 * usable_length

def toy_with_volume(toy):
    for size in toy:
        volume = get_usable_volume_for_size(toy[size])
        toy[size]['calculated-usable-volume'] = volume

    return toy

def toys_with_volume(toys):
    for toy in toys:
        toys[toy] = toy_with_volume(toys[toy])

    return toys

def get_toy_key_names(toys):
    toy_keys = []

    for toy in toys:
        for size in toys[toy]:
            for key in toys[toy][size]:
                if key not in toy_keys:
                    toy_keys.append(key)
    
    return toy_keys

def get_toy_list(toys):
    toy_list = []

    for toy in toys:
        for size in toys[toy]:
            toy_list.append({
                'name': toy,
                'size': size,
                'dimensions': toys[toy][size]
                })

    return toy_list

def main():
    # toys = json.load(open('toys.txt'))

    # toys = toys_to_metric(toys)
    # toys = toys_with_volume(toys)

    toys = toys_with_volume(
            toys_to_metric(
                json.load(
                    open('toys.txt'))))

    toy_keys = get_toy_key_names(toys)

    toy_list = get_toy_list(toys)

    exported_toys = { 'keys': toy_keys, 'list': toy_list }

    json.dump(exported_toys, open('exported_toys.txt', 'w'))


if __name__ == '__main__':
    main()
