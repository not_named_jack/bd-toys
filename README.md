# Bad Dragon Toys

This repository contains a few python scripts to scrape and process toys from
the Bad Dragon [realsize app](https://bad-dragon.com/real-size).

`main.py` all the toys using headless Chrome and writes them to `toys.txt`.

`toys.py` reads from `toys.txt`, converts all the toys to metric, estimates the
volume of each toy, and dumps the toys to `toys.txt` again.

In `public/toy_data.js` there should be a variable named `TOY_DATA` with the
contents of `toys.txt`