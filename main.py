# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
from selenium.webdriver import Chrome
from selenium.webdriver import ChromeOptions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as expected
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.select import Select

import time
import json

def wait_for_element_by_css_class(wait, css_class):
    return wait.until(expected.visibility_of_element_located((By.CSS_SELECTOR, css_class)))

def wait_for_element_by_xpath(wait, xpath):
    return wait.until(expected.visibility_of_element_located(
        (By.XPATH, "//li[contains(text(), \"{}\")]".format(xpath))))

def get_toys(wait):
    wait.until(expected.visibility_of_element_located((By.ID, 'inventory-filters-sku'))).click()
    toys = driver.find_elements_by_class_name('autocomplete__item')

    toy_names = []
    for toy in toys:
        toy_names.append(toy.text)

    return toy_names

def select_toy(wait, toy_name):
    driver.find_element_by_id('inventory-filters-sku').click()

    wait_for_element_by_xpath(wait, toy_name).click()

def close_current_toy(wait):
    wait_for_element_by_css_class(wait, 'div.autocomplete__pill__close').click()

def get_toy_sizes(wait, toy):
    wait_for_element_by_css_class(wait, 'div.real-size__toy__description')
    # wait.until(expected.visibility_of_element_located((By.CSS_SELECTOR, 'div.real-size__toy__description')))

    toy_dropdown = Select(wait.until(expected.visibility_of_element_located((By. ID, 'real-size__toy__size-dropdown'))))
    options = [o.text for o in toy_dropdown.options]

    sizes = {}

    for option in options:
        print('{} / {}'.format(options.index(option) + 1, len(options)))
        toy_dropdown.select_by_visible_text(option)
        option = option.lower().replace(' ', '-')
        wait_for_element_by_css_class(wait, 'th.reactable-th-{}'.format(option))
        table = driver.find_element_by_class_name('real-size__toy__sizing-chart__table')
        table_without_first_row = table.text.split('\n')[1:]

        sizes[option] = {}

        for row in table_without_first_row:
            split = row.split(' ')
            value = float(split[-1].replace('"', ''))
            property = '-'.join(split[:-1]).lower()

            sizes[option][property] = value

        time.sleep(1)

    return sizes

if __name__ == "__main__":
    options = ChromeOptions()
    options.add_argument('--headless')
    driver = Chrome(executable_path='chromedriver', chrome_options=options)
    wait = WebDriverWait(driver, timeout=10)

    print("Getting toy app")
    driver.get('https://bad-dragon.com/real-size?')

    print("Getting toys")
    
    toys = get_toys(wait)
    amount_of_toys = len(toys)

    toy_sizes = {}


    for toy in toys[::-1]:
        print('{} (toy {} / {})'.format(toy, toys.index(toy) + 1, amount_of_toys))
        select_toy(wait, toy)
        toy_sizes[toy] = get_toy_sizes(wait, toy)
        time.sleep(1)
        close_current_toy(wait)

    json.dump(toy_sizes, open('toys.txt', 'w'))
    print('Toys written to file')

    driver.quit()
